# tp_final_tests

To run the part 1 and part 2 tests run :

```
npm run test
```

To run the part 3 tests run :

```
npm run bdd
```

Screenshot of the results of part 3 and 4 in root directory