function calculateBudget(income, expenses) {
    if (!Array.isArray(expenses)) throw new Error("Expenses must be an array");
 
    const totalExpenses = expenses.reduce((acc, expense) => acc + expense, 0);
    return income - totalExpenses;
 }
 
 module.exports = calculateBudget; 