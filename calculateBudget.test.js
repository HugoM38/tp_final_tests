const calculateBudget = require('./calculateBudget.js');

describe('calculateBudget', () => {
   test('calculates budget correctly with valid inputs', () => {
      expect(calculateBudget(5000, [150, 425, 157])).toBe(4268);
   });

   test('throws an error if expenses is not an array', () => {
      expect(() => calculateBudget(1000, 10)).toThrow("Expenses must be an array");
   });

   test('returns income if expenses is an empty array', () => {
      expect(calculateBudget(1000, [])).toBe(1000);
   });

   test('handles negative expenses correctly', () => {
      expect(calculateBudget(1000, [-200, -300])).toBe(1500);
   });
});