const express = require('express');
const app = express();

app.use(express.json());

app.post('/login', (req, res) => {
   const { username, password } = req.body;

   if (!username || !password) {
      return res.status(400).json({ error: 'Bad Request' });
   }

   if (username === 'user' && password === 'pass') {
      return res.status(200).json({ token: 'xyz' });
   } else {
      return res.status(401).json({ error: 'Unauthorized' });
   }
});

module.exports = app;