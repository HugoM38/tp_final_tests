const request = require('supertest');
const app = require('./app');

describe('POST /login', () => {
   it('must responds with 200 OK and a token for valid credentials', async () => {
      const response = await request(app)
         .post('/login')
         .send({ username: 'user', password: 'pass' });
      expect(response.status).toBe(200);
      expect(response.body).toHaveProperty('token', 'xyz');
   });

   it('must responds with 401 Unauthorized for invalid credentials', async () => {
      const response = await request(app)
         .post('/login')
         .send({ username: 'user', password: 'wrongpass' });
      expect(response.status).toBe(401);
      expect(response.body).toHaveProperty('error', 'Unauthorized');
   });

   it(' must responds with 400 Bad Request for bad payloads', async () => {
      const response = await request(app)
         .post('/login')
         .send({});
      expect(response.status).toBe(400);
      expect(response.body).toHaveProperty('error', 'Bad Request');
   });
});
