const { Given, When, Then } = require('@cucumber/cucumber');
const request = require('supertest');
const app = require('../../app');
const assert = require('assert');

let response;

Given('the user provides valid credentials', function () {
    this.credentials = { username: 'user', password: 'pass' };
});

Given('the user provides invalid credentials', function () {
    this.credentials = { username: 'user', password: 'wrongpass' };
});

When('the user sends a POST request to login endpoint', async function () {
    response = await request(app)
        .post('/login')
        .send(this.credentials);
});

Then('the response should be {int} OK', function (statusCode) {
    assert.strictEqual(response.status, statusCode);
});

Then('the response should be {int} Unauthorized', function (statusCode) {
    assert.strictEqual(response.status, statusCode);
});