Feature: User Login

  Scenario: Successful login with valid credentials
    Given the user provides valid credentials
    When the user sends a POST request to login endpoint
    Then the response should be 200 OK

  Scenario: Failed login with invalid credentials
    Given the user provides invalid credentials
    When the user sends a POST request to login endpoint
    Then the response should be 401 Unauthorized
